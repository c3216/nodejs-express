const { Router } = require("express")
const router = Router();

const cocktailController = require("../controllers/cocktailController")


router.get('/random', cocktailController.getRandom)

module.exports = router