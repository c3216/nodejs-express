const userController = {}
const users = [
  {id: 1, name: 'Daniel', email: 'daniel@mail.com'},
  {id: 2, name: 'Javier', email: 'javier@mail.com'},
  {id: 3, name: 'Adrian', email: 'adrian@mail.com'}
]

userController.getUsers = (req, res) => {
  res.json(users)
}

userController.getUserById = (req, res) => {
  const { id } = req.params

  let index = users.findIndex((u) => u.id == id)

  if (index > -1) {
    res.json(users[index])
  } else {
    res.status(404).json({message: "No se encontro el usuario"})
  }
}

userController.postUser = (req, res) => {
  const { id, name, email } = req.body

  // users.push({id, name, email})
  let user = {id: id, name: name, email: email}
  users.push(user)

  res.json({message: "se creo correctamente el usuario", user: user})

}

userController.putUser = (req, res) => {
  const { id } = req.params
  const { name, email } = req.body

  // for (let i = 0; i < users.length; i++) {
  //   if (users[i].id == id) {
  //     users[i] = {id, name, email}
  //   }
  // }

  let index = users.findIndex((u) => u.id == id)

  if (index > -1) {
    users[index] = {id: +id, name, email}
    res.json({message: "se actualizo el usuario correctamente"})
  } else {
    res.status(404).json({message: "No se encontro el usuario"})
  }
}

userController.deleteUser = (req, res) => {
  const { id } = req.params

  let index = users.findIndex((u) => u.id == id)
  if (index > -1) {
    users.splice(index, 1)
    res.json({message: "Se elimino correctamente el usuario"})
  } else {
    res.status(404).json({message: "No se encontro el usuario"})
  }
}

module.exports = userController