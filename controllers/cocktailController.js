const axios = require("axios");
const cocktailController = {};

cocktailController.getRandom = async (req, res) => {
  // fetch("https://www.thecocktaildb.com/api/json/v1/1/random.php")
  //   .then((response) => {
  //     return response.json();
  //   })
  //   .then((data) => {
  //     res.json(data);
  //   })
  //   .catch((error) => {
  //     res.status(500).json({ message: "Error en peticion" });
  //   });

  try {
    let response = await axios.get(
      "https://www.thecocktaildb.com/api/json/v1/1/random.php"
    );
    let response = await axios.post("", {}, {headers: {
      'Content-Type': 'application/json'
    }})
    console.log(response)
    res.send(response.data);
  } catch (error) {
    res.status(500).json({ message: error });
  }

  // axios
  //   .get("https://www.thecocktaildb.com/api/json/v1/1/random.php")
  //   .then((response) => {
  //     console.log(response.data)
  //     res.send(response.data);
  //   })
  //   .catch((error) => {
  //     res.status(500).json({ message: error });
  //   });
};

module.exports = cocktailController;
