const express = require("express")
const app = express()

const { json, urlencoded } = require("body-parser");
// const bodyParser = require("body-parser");

const cors = require("cors");
const morgan = require("morgan");

const userRoute = require("./routes/userRoutes.js")
const cocktailRoute = require("./routes/cocktailRoutes.js")

app.set("port", 3001)

app.use(cors())
app.use(json());
app.use(urlencoded({extended: false}));

app.use(morgan("dev"))

app.use("/users", userRoute)
app.use("/cocktails", cocktailRoute)


app.get('/', (req, res) => {
  res.send('Esto es express hola')
})


module.exports = app